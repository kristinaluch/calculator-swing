package test.com.calculator.services;

import main.com.calculator.services.CalculatorService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

public class CalculatorServiceTest {

    private static final CalculatorService cut = new CalculatorService();

    static Arguments[] calculateNumbersTestArgs(){
        return new Arguments[]{
                Arguments.arguments("-2.0", "5", "-7", "+"),
                Arguments.arguments("1.5","3","2","/"),
                Arguments.arguments("0.8", "7", "6.2", "-"),
                Arguments.arguments("6.0", "2", "3", "*"),
                Arguments.arguments("0.33", "1","3", "/"),
                Arguments.arguments("Incorrect number", "1g","3", "/"),
                Arguments.arguments("Incorrect symbol", "2", "3", "plus"),
                Arguments.arguments("Division by zero is not possible", "1","0", "/"),
        };
    }

    @ParameterizedTest
    @MethodSource("calculateNumbersTestArgs")
    void calculateNumbersTest(String expected, String textNumber1, String textNumber2, String symbol){
//        Mockito.when(validation.getNumber()).thenReturn(number1, number2);
//        Mockito.when(validation.getSymbol()).thenReturn(symbol);
        String actual = cut.calculateNumbers(textNumber1, textNumber2, symbol);
        Assertions.assertEquals(expected, actual);
    }

}
