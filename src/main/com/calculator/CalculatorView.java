package main.com.calculator;

import main.com.calculator.services.CalculatorService;


import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Scanner;


public class CalculatorView extends JFrame{

    private CalculatorService calculatorService;

    private  JButton buttonCalculate = new JButton("���������");
    private JTextField inputNumber1 = new JTextField(5);
    private JTextField inputNumber2 = new JTextField(5);
    private JTextField inputOperation = new JTextField(5);

    private JLabel labelNumber1 = new JLabel("����� 1");
    private JLabel labelNumber2 = new JLabel("����� 2");
    private JLabel labelOperation = new JLabel("��������");
    private JLabel labelResult = new JLabel("���������");
    private JTextField  outputResult = new JTextField ("");

    public CalculatorView (CalculatorService calculatorService){
        super("Calculator");
        this.setBounds(400, 400, 300,350 );
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Container container = this.getContentPane();
        container.setLayout(new GridLayout(9,1));
        container.add(labelNumber1);
        container.add(inputNumber1);

        container.add(labelNumber2);
        container.add(inputNumber2);
        container.add(labelOperation);
        container.add(inputOperation);
        buttonCalculate.addActionListener(new ButtonEventListener());
        container.add(buttonCalculate);
        container.add(labelResult);
        container.add(outputResult);
        this.calculatorService = calculatorService;
    }


    class ButtonEventListener implements ActionListener{
        public void actionPerformed(ActionEvent event) {
            String str1 = inputNumber1.getText();
            String str2 = inputNumber2.getText();
            String operation = inputOperation.getText();
            outputResult.setText(calculatorService.calculateNumbers(str1, str2, operation));
        }
    }

}
