package main.com.calculator.services;



public class CalculatorService {



    private static final String NOT_SYMBOL = "Incorrect symbol";
    private static final String BY_ZERO = "Division by zero is not possible";
    private static final String NOT_NUMBER = "Incorrect number";

    public String calculateNumbers(String textNumber1, String textNumber2, String symbol){
        double number1 = getNumber(textNumber1);

        double number2 = getNumber(textNumber2);
        if (Double.isNaN(number1)||Double.isNaN(number2)){
            return NOT_NUMBER;
        }
        double result;

        if (symbol.equals("+")) {
            result = number1 + number2;
        }
        else if (symbol.equals("-")) {
            result = number1 - number2;
        }
        else if (symbol.equals("*")) {
            result = number1*number2;
        }
        else if (symbol.equals("/")) {
            if (number2 == 0)
                return BY_ZERO;
            else {
                result = number1 / number2;
            }
        }
        else{
            return NOT_SYMBOL;
        }
        return printResult(result);
    }

    private String printResult(double result){
        result = Math.round(result*100);
        result = result/100;
        String printResult = "" + result;
        return printResult;
    }

    public double getNumber(String text){
        double number;
        boolean digit = text.matches("(-*\\d*\\.\\d*)||0||-*\\d*");

        if (digit) {
            number = Double.parseDouble(text);
        } else {
            return Double.NaN;
        }
        return number;
    }
}
