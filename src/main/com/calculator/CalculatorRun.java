package main.com.calculator;

import main.com.calculator.services.CalculatorService;

import java.util.Scanner;

public class CalculatorRun {
    public static void main(String[] args) {
        //Scanner scanner = new Scanner(System.in);

        CalculatorService calculatorService = new CalculatorService();
        CalculatorView calculatorView = new CalculatorView(calculatorService);
        calculatorView.setVisible(true);

           // System.out.println(calculatorService.calculateNumbers());

    }
}
